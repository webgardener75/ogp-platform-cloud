## Synopsis

This project aims to deploy consultations tools (such as DemocracyOS, Consul or Madison) for the French Administrations. It is connected to the ogptoolbox-platform project. 

## Technology 

The tools are deployed on a cloud managed by OpenStack. 

## What it does

A user fills a form for a given tool : https://consultation.etalab.gouv.fr/tools/democracyos for example.
After mail confirmation, an instance of the tools is deployed on the cloud. The URL of the consultation will be : https://exemple.consultation.etalab.gouv.fr

## Technicals requirements to add a tool  

An instanciable ubuntu-based (or debian based) image (qcow2 or raw) of your software is required. 
An instance image is a single file which contains a virtual disk that has a bootable operating system installed on it

This image shall contains a few additionnals stuff to work on the deployment platform :
- a ogptoolbox user with sudo rights
- put the platform public key in authorized_keys :
<pre>
mkdir /home/ogptoolbox/.ssh/
touch /home/ogptoolbox/.ssh/authorized_keys
chown -R ogptoolbox:ogptoolbox /home/ogptoolbox/.ssh/
</pre>
and then put the content of id_rsa.pub (below) in authorized_keys file :
<pre>
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCss0Ba2bWKGuatjOFdgONjQ4P9n3ad1WjNSksacfjYzEDIpAli18oxWeLcQa3GC+t4aiXObNu7pR2acwkVkFdKhSWdQSYGYBJ6zAwGlqdzNCLklY3dibrt4riRvSySi+W8ufOFx/CP+numS5MkO2cLRalgOtCDMrGnD2YZQr1y2xawRGe/ZC+q2t2Rk8odHffRHb7bndIjEBrZFti9FwV36bI/kQtiob/ULL6IufTFr4k+XXo7VnZ9K0tDxfkSDTxls3QkIiN459vWk+JYTehXi3c94gCNf6t/FmOuKPquNvy8u5aC2TP95nFgiTu7CNt4jHkjfB+k+KT+UCuV6vCD ogptoolbox@vps326869.ovh.net
</pre>

